# Ansible role - Deploy Frontend
This role provides installation of Cryton Frontend using Docker.  
Docker is also installed in this role.

## Supported platforms

| Platform | version   |
|----------|-----------|
| Debian   | \>=11     |
| Kali     | \>=2022.1 |

## Requirements

- Root access (specify `become` directive as a global or while invoking the role)
    ```yaml
    become: yes
    ```
- Ansible variables (do not disable `gather_facts` directive)

## Parameters
The parameters and their defaults can be found in the `defaults/main.yml` file.

| variable                | description           |
|-------------------------|-----------------------|
| cryton_frontend_version | Which version to use. |

## Examples

### Usage
```yaml
  roles:
    - role: deploy-frontend
      become: yes

```

### Inclusion

[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies)
```yaml
- name: deploy-frontend
  src: https://gitlab.ics.muni.cz/cryton/ansible/deploy-frontend.git
  version: "master"
  scm: git
```
